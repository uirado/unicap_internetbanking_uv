package br.unicap.ib.gui;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author uira
 */

public class JdbcUtil {
    
    private static Connection con;
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static final String URL = "jdbc:mysql://localhost:3306/ib_db";
    private static final String USER = "root";
    private static final String PASS = "1234";
    
    static {
        
        try {
            Class.forName (DRIVER);
            con = DriverManager.getConnection (URL, USER, PASS);
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(JdbcUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static Connection getConnection(){
        return con;
    }

}
