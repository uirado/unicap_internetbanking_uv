package br.unicap.ib.gui;

import br.unicap.ib.dados.RepositorioClienteSet;
import br.unicap.ib.dados.RepositorioClientesJDBC;
import br.unicap.ib.dados.RepositorioContasJDBC;
import br.unicap.ib.dados.RepositorioContasMap;
import br.unicap.ib.negocio.CadastroCliente;
import br.unicap.ib.negocio.CadastroConta;
import br.unicap.ib.negocio.Cliente;
import br.unicap.ib.negocio.ContaAbstrata;
import br.unicap.ib.exceptions.ClienteExistenteException;
import br.unicap.ib.exceptions.ClienteInexistenteException;
import br.unicap.ib.exceptions.ContaExistenteException;
import br.unicap.ib.exceptions.ContaInexistenteException;
import br.unicap.ib.exceptions.SaldoInsuficienteException;

/**
 *
 * @author uira
 */
public class Fachada {
    
    private static Fachada instancia;
    private CadastroCliente cadClientes;
    private CadastroConta cadContas;
    
    
    private Fachada() {
        cadClientes = new CadastroCliente(new RepositorioClientesJDBC());
        cadContas = new CadastroConta(new RepositorioContasJDBC());
    }
    
    public static Fachada getInstance() {
        if(instancia == null) {
            instancia = new Fachada();
        }
        return instancia;
    }
    
    
    //MÉTODOS CRUD DE CONTA
    public void inserirConta(ContaAbstrata conta) throws ContaExistenteException {
        try {
            cadContas.inserir(conta);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    
    public void removerConta(String numeroConta) throws ContaInexistenteException {
        try {
            cadContas.remover(numeroConta);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }   
    }
    
    public void atualizarConta(ContaAbstrata conta) throws ContaInexistenteException {
        try {
            cadContas.atualizar(conta);
        } catch (Exception e) {
            
        }
    }
    
    public ContaAbstrata consultarConta(String numeroConta) throws ContaInexistenteException {
        return cadContas.consultar(numeroConta);
    }
    
    //MÉTODOS DE REGRA DE NEGÓCIO DE CONTA
    public void creditar(String numero, double valor) throws ContaInexistenteException {
        try {
            cadContas.creditar(numero, valor);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void debitar(String numero, double valor) throws ContaInexistenteException, SaldoInsuficienteException {
        try {
            cadContas.debitar(numero, valor);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void transferir(String numeroOrigem, String numeroDestino, double valor)
            throws SaldoInsuficienteException, ContaInexistenteException {
        try {
            cadContas.transferir(numeroOrigem, numeroDestino, 0);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        
    }
    
    public void renderJuros(ContaAbstrata conta) {
        cadContas.renderJuros(conta);
    }
    
    public void renderBonus(ContaAbstrata conta) {
        cadContas.renderBonus(conta);
    }
    
    
    //METODOS CRUD DE CLIENTE
    public void inserirCliente(String cpf, String nome) throws ClienteExistenteException {
        try {
            cadClientes.inserir(new Cliente(cpf, nome));    
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        
    }
    
    public void inserirCliente(Cliente cliente) throws ClienteExistenteException {
        try {
            cadClientes.inserir(cliente);
        } catch (Exception e) {
            e.getMessage();
        }
    }
    
    public void removerCliente(String cpf) throws ClienteInexistenteException {
        try {
            cadClientes.remover(cpf);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void atualizarCliente(Cliente cliente) throws ClienteInexistenteException {
        try {
            cadClientes.atualizar(cliente);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    public Cliente consultarCliente(String cpfCliente) throws ClienteInexistenteException {
        return cadClientes.consultar(cpfCliente);
    }

    void listarClientes() {
        cadClientes.listar();
    }
    
}
