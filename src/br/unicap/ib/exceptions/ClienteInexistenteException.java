package br.unicap.ib.exceptions;

/**
 *
 * @author uira
 */
public class ClienteInexistenteException extends Exception{

    public ClienteInexistenteException() {
        super("Cliente não cadastrado.");
    }
    
}
