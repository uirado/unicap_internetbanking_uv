package br.unicap.ib.exceptions;

/**
 *
 * @author uira
 */
public class ContaExistenteException extends Exception {

    public ContaExistenteException() {
        super("Conta já cadastrada.");
    }
    
}
