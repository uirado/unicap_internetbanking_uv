package br.unicap.ib.exceptions;

/**
 *
 * @author uira
 */
public class SaldoInsuficienteException extends Exception{

    public SaldoInsuficienteException() {
        super("Saldo insuficiente.");
    }

}
