package br.unicap.ib.exceptions;

/**
 *
 * @author uira
 */
public class ContaInexistenteException extends Exception {

    public ContaInexistenteException() {
        super("Conta não cadastrada.");
    }
    
    
    
}
