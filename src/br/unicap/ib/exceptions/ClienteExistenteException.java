package br.unicap.ib.exceptions;

/**
 *
 * @author uira
 */
public class ClienteExistenteException extends Exception{

    public ClienteExistenteException() {
        super("Cliente já cadastrado.");
    }
    
    
}
