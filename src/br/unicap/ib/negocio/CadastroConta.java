package br.unicap.ib.negocio;

import br.unicap.ib.dados.IRepositorioContas;
import br.unicap.ib.dados.RepositorioContasArray;
import br.unicap.ib.exceptions.ContaExistenteException;
import br.unicap.ib.exceptions.ContaInexistenteException;
import br.unicap.ib.exceptions.SaldoInsuficienteException;

/**
 *
 * @author uira
 */
public class CadastroConta {
    private IRepositorioContas rep;

    public CadastroConta(IRepositorioContas repositorio) {
        rep = repositorio;
    }
        
    // ======== METODOS DE REGRA DE NEGOCIO =======
    public void creditar(String numero, double valor) throws ContaInexistenteException {
        ContaAbstrata c = rep.consultar(numero);
        c.creditar(valor);
        atualizar(c);
    }
    
    public void debitar(String numero, double valor) throws ContaInexistenteException, SaldoInsuficienteException {
        ContaAbstrata c = rep.consultar(numero);
        c.debitar(valor);
        atualizar(c);
    }
    
    public void transferir(String numeroOrigem, String numeroDestino, double valor) 
            throws SaldoInsuficienteException, ContaInexistenteException {
        
        ContaAbstrata contaOrigem = rep.consultar(numeroOrigem);
        ContaAbstrata contaDestino = rep.consultar(numeroDestino);
        
        contaOrigem.tranferir(contaDestino, valor);
        atualizar(contaOrigem);
        atualizar(contaDestino);
    }
    
    public void renderJuros(ContaAbstrata conta) {
        if(conta instanceof Poupanca) {
            ((Poupanca) conta).renderJuros();
        }
    }
    
    public void renderBonus(ContaAbstrata conta) {
        if(conta instanceof ContaBonificada) {
            ((ContaBonificada) conta).renderBonus();
        }
    }
    // ============================================
    
    // ========= METODOS CRUD =========
    public void inserir(ContaAbstrata conta) throws ContaExistenteException{
        try {
            rep.inserir(conta);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void remover(String numeroConta) throws ContaInexistenteException{
        rep.remover(numeroConta);
    }
    
    public ContaAbstrata consultar(String numeroConta) throws ContaInexistenteException{
        return rep.consultar(numeroConta);
    }
    
    public void atualizar(ContaAbstrata conta) throws ContaInexistenteException{
        rep.atualizar(conta);
    }
    
}
