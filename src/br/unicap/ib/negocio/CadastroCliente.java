package br.unicap.ib.negocio;

import br.unicap.ib.dados.IRepositorioClientes;
import br.unicap.ib.exceptions.ClienteExistenteException;
import br.unicap.ib.exceptions.ClienteInexistenteException;
import java.util.List;

/**
 *
 * @author uira
 */
public class CadastroCliente {
    private IRepositorioClientes rep;

    public CadastroCliente(IRepositorioClientes repositorio) {
        rep = repositorio;
    }
      
    // ======== METODOS CRUD ==========
    
    public void inserir(Cliente cliente) throws ClienteExistenteException{
        rep.inserir(cliente);
    }
    
    public void remover(String cpf) throws ClienteInexistenteException {
        rep.remover(cpf);
    }
    
    public Cliente consultar(String cpfCliente) throws ClienteInexistenteException {
        return rep.consultar(cpfCliente);
    }
    
    public void atualizar(Cliente cliente) throws ClienteInexistenteException {
        rep.atualizar(cliente);
    }
    // =========================================
    
    public void listar() {
        List<Cliente> clientes = rep.getAll();
        for(Cliente c : clientes) {
            System.out.println(c.toString());
        }
    }
}
