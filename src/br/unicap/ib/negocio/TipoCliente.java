package br.unicap.ib.negocio;

/**
 *
 * @author uira
 */
public enum TipoCliente {
    VIP,
    CLASS,
    STANDARD
}
