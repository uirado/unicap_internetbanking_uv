package br.unicap.ib.negocio;

/**
 *
 * @author uira
 */

public class Cliente implements Comparable<Cliente>{
    
    private String cpf;
    private String nome;
    private Endereco endereco;
    private TipoCliente tipo;

    public Cliente(String cpf, String nome) {
        this.cpf = cpf;
        this.nome = nome;
    }

    public TipoCliente getTipo() {
        return tipo;
    }

    public void setTipo(TipoCliente tipo) {
        this.tipo = tipo;
    }

    public Cliente(String nome, String cpf, String rua, String numero, String bairro, String cep) {
        this.nome = nome;
        this.cpf = cpf;
        this.endereco = new Endereco(rua, numero, bairro, cep);
    }

    public String getCpf() {
        return cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }
    
    public boolean equals(Cliente c) {
        return cpf.equals(c.getCpf());
    }

    @Override
    public int compareTo(Cliente outroCliente) {
        return nome.compareTo(outroCliente.getNome());
    }

    @Override
    public String toString() {
        return getNome();
    }
    
    
}
