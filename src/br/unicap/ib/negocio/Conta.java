package br.unicap.ib.negocio;

import br.unicap.ib.exceptions.SaldoInsuficienteException;

/**
 *
 * @author uira
 */
public class Conta extends ContaAbstrata{

    public Conta(String numero, Cliente cliente) {
        super(numero, cliente);
    }
    
    public Conta(String numero, double saldo, Cliente cliente) {
        super(numero, saldo, cliente);
    }

    @Override
    public void debitar(double valor) throws SaldoInsuficienteException {
        double novoSaldo = getSaldo();
        if(novoSaldo >= valor) {
            novoSaldo -= valor;
            setSaldo(novoSaldo);
        } else{
            throw new SaldoInsuficienteException();
        }
    }
    

}
