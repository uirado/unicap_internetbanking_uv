package br.unicap.ib.negocio;

/**
 *
 * @author uira
 */
public class Poupanca extends Conta{
    
    private static final double TAXA_RENDIMENTO = 0.01;

    public Poupanca(String numero, Cliente cliente) {
        super(numero, cliente);
    }

    public Poupanca(String numero, double saldo, Cliente cliente) {
        super(numero, saldo, cliente);
    }
    
    public static double getTAXA_RENDIMENTO() {
        return TAXA_RENDIMENTO;
    }
    
    //REGRASDE NEGOCIO
    public void renderJuros() {
        double saldoAtual = getSaldo();
        creditar(saldoAtual * TAXA_RENDIMENTO);
    }

}
