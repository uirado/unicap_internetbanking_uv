package br.unicap.ib.negocio;

import br.unicap.ib.exceptions.SaldoInsuficienteException;

/**
 *
 * @author uira
 */
public abstract class ContaAbstrata {
    
    private String numero;
    private double saldo;
    private Cliente cliente;
    

    public ContaAbstrata(String numero, Cliente cliente) {
        this(numero, 0, cliente);
    }
    
    public ContaAbstrata(String numero, double saldo, Cliente cliente) {
        this.numero = numero;
        this.saldo = saldo;
        this.cliente = cliente;
    }
    
    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    
    public String getNumero() {
        return numero;
    }

    public double getSaldo() {
        return saldo;
    }
    
    public void setSaldo(double valor) {
        this.saldo = valor;
    }
    
    //REGRAS DE NEGOCIO
    public void creditar(double valor) {
        saldo += valor;
    }

    public abstract void debitar(double valor) throws SaldoInsuficienteException;
    
    public void tranferir(ContaAbstrata contaDestino, double valor) throws SaldoInsuficienteException {
        debitar(valor);
        contaDestino.creditar(valor);
    }
    
    public boolean equals(ContaAbstrata c) {
        return numero.equals(c.getNumero());
    }

}
