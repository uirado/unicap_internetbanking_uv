package br.unicap.ib.negocio;

import br.unicap.ib.exceptions.SaldoInsuficienteException;

/**
 *
 * @author uira
 */
public class ContaImposto extends ContaAbstrata{
    
    private static final double taxa = 0.01;

    public ContaImposto(String numero, Cliente cliente) {
        super(numero, cliente);
    }

    public ContaImposto(String numero, double saldo, Cliente cliente) {
        super(numero, saldo, cliente);
    }

    @Override
    public void debitar(double valor) throws SaldoInsuficienteException {
        double novoSaldo = getSaldo();
        
        novoSaldo -= valor + valor*taxa;
        if(novoSaldo >= 0) {
            setSaldo(novoSaldo);
        } else {
            throw new SaldoInsuficienteException();
        }
    }
    
}
