package br.unicap.ib.negocio;

/**
 *
 * @author uira
 */
public class ContaBonificada extends Conta{
    
    private double bonus;
    private static final double taxaBonus = 0.01;

    public ContaBonificada(String numero, Cliente cliente) {
        super(numero, cliente);
    }
    
    public ContaBonificada(String numero, double saldo, Cliente cliente) {
        super(numero, saldo, cliente);
        this.bonus = 0;
    }
    
    public double getBonus() {
        return bonus;
    }
    
    
    //REGRAS DE NEGOCIO
    @Override
    public void creditar(double valor) {
        bonus += taxaBonus * valor;
        super.creditar(valor); 
    }
    
    public void renderBonus() {
        super.creditar(bonus);
        bonus = 0;
    }

}
