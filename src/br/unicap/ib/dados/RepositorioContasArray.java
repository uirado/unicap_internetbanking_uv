package br.unicap.ib.dados;

import br.unicap.ib.exceptions.ContaExistenteException;
import br.unicap.ib.negocio.ContaAbstrata;
import br.unicap.ib.exceptions.ContaInexistenteException;

/**
 *
 * @author uira
 */
public class RepositorioContasArray implements IRepositorioContas{
    
    public static final int TAM_CACHE_CONTAS = 100;
    private ContaAbstrata[] contas;
    private int indice;

    public RepositorioContasArray() {
        contas = new ContaAbstrata[TAM_CACHE_CONTAS];
        indice = 0;
    }

    @Override
    public void inserir(ContaAbstrata conta) throws ContaExistenteException {
        if(!existe(conta.getNumero())) {
            contas[indice] = conta;
            indice++;
        } else {
            throw new ContaExistenteException();
        }
    }
    
    @Override
    public void atualizar(ContaAbstrata conta) throws ContaInexistenteException {
        int i = procurarIndice(conta.getNumero());
        if(i != -1) {
            contas[i] = conta;
        } else {
            throw new ContaInexistenteException();
        }
    }
    
    @Override
    public void remover(String numeroConta) throws ContaInexistenteException {
        int i = procurarIndice(numeroConta);
        if (i != -1) {
            contas[i] = null;
            defragArray(i);
        } else {
            throw new ContaInexistenteException();
        }
    }
    
    private int procurarIndice(String numeroConta) {
        for(int i = 0; i < indice; i++) {
            if(contas[i].getNumero().equals(numeroConta)) {
                return i;
            }
        }
        return -1;
    }
    
    @Override
    public boolean existe(String numeroConta) {
        int i = procurarIndice(numeroConta);
        if(i != -1) {
            return true;
        }
        return false;
    }
    
    @Override
    public ContaAbstrata consultar (String numeroConta) throws ContaInexistenteException{
        if(existe(numeroConta)) {
            int i = procurarIndice(numeroConta);
            return contas[i];
        } else {
            throw new ContaInexistenteException();
        }
    }

    private void defragArray(int k) {
        contas[k] = contas[indice-1];
        contas[indice-1] = null;
        indice--;
    }
    
}
