package br.unicap.ib.dados;

import br.unicap.ib.negocio.Conta;
import br.unicap.ib.negocio.ContaAbstrata;
import br.unicap.ib.exceptions.ContaExistenteException;
import br.unicap.ib.exceptions.ContaInexistenteException;

/**
 *
 * @author uira
 */
public interface IRepositorioContas {
     
    public void inserir(ContaAbstrata conta) throws ContaExistenteException ;
    
    public void atualizar(ContaAbstrata conta) throws ContaInexistenteException;
    
    public void remover(String numeroConta) throws ContaInexistenteException;
    
    public ContaAbstrata consultar (String numeroConta) throws ContaInexistenteException;
    
    public boolean existe(String numeroConta);
}
