package br.unicap.ib.dados;

import br.unicap.ib.negocio.Cliente;
import br.unicap.ib.exceptions.ClienteExistenteException;
import br.unicap.ib.exceptions.ClienteInexistenteException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author uira
 */

public class RepositorioClientesArray implements IRepositorioClientes{
    private static final int TAM_CACHE_CLIENTES = 100;
    private Cliente[] clientes;
    private int indice;

    public RepositorioClientesArray() {
        clientes = new Cliente[TAM_CACHE_CLIENTES];
        indice = 0;
    }
    
    @Override
    public void inserir(Cliente cliente) throws ClienteExistenteException {
        if(!existe(cliente.getCpf())) {
            clientes[indice] = cliente;
            indice++;
        } else {
            throw new ClienteExistenteException();
        }
    }
    
    @Override
    public void atualizar(Cliente cliente) throws ClienteInexistenteException {
        int i = procurarIndice(cliente.getCpf());
        if(i != -1) {
            clientes[i] = cliente;
        } else {
            throw new ClienteInexistenteException();
        }
    }
    
    public void remover(String cpf) throws ClienteInexistenteException {
        int i = procurarIndice(cpf);
        if (i != -1) {
            clientes[i] = null;
            defragArray(i);
        } else {
            throw new ClienteInexistenteException();
        }
    }
    
    private int procurarIndice(String cpfCliente) {
        for(int i = 0; i < indice; i++) {
            if(clientes[i].getCpf().equals(cpfCliente)) {
                return i;
            }
        }
        return -1;
    }
    
    @Override
    public boolean existe(String cpfCliente) {
        int i = procurarIndice(cpfCliente);
        if(i != -1) {
            return true;
        }
        return false;
    }
    
    @Override
    public Cliente consultar (String cpfCliente) throws ClienteInexistenteException {
        if(existe(cpfCliente)) {
            int i = procurarIndice(cpfCliente);
            return clientes[i];
        } else {
            throw new ClienteInexistenteException();
        }
    }

    private void defragArray(int k) {
        clientes[k] = clientes[indice-1];
        clientes[indice-1] = null;
        indice--;
    }

    @Override
    public List<Cliente> getAll() {
        List<Cliente> lista = new ArrayList<>();
        for(int i = 0; i < indice; i++){
            lista.add(clientes[i]);
        }
        return lista;
    }

}
