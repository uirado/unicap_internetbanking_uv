package br.unicap.ib.dados;

import br.unicap.ib.negocio.ContaAbstrata;
import br.unicap.ib.exceptions.ContaExistenteException;
import br.unicap.ib.exceptions.ContaInexistenteException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author uira
 */
public class RepositorioContasMap implements IRepositorioContas {
    
    private Map<String, ContaAbstrata> contas;

    public RepositorioContasMap() {
        contas = new HashMap<>();
    }
    
    @Override
    public void inserir(ContaAbstrata conta) throws ContaExistenteException {
        if(!existe(conta.getNumero())){
            contas.put(conta.getNumero(), conta);
        } else {
            throw new ContaExistenteException();
        }
    }

    @Override
    public void atualizar(ContaAbstrata conta) throws ContaInexistenteException {
        remover(conta.getNumero());
        contas.put(conta.getNumero(), conta);
    }

    @Override
    public void remover(String numeroConta) throws ContaInexistenteException {
        if(existe(numeroConta)) {
            contas.remove(numeroConta);
        } else {
            throw new ContaInexistenteException();
        }
    }

    @Override
    public ContaAbstrata consultar(String numeroConta) throws ContaInexistenteException {
        if(existe(numeroConta)) {
            return contas.get(numeroConta);
        } else {
            throw new ContaInexistenteException();
        }
        
    }

    @Override
    public boolean existe(String numeroConta) {
        return contas.containsKey(numeroConta);
    }
    
}
