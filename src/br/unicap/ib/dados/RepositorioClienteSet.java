package br.unicap.ib.dados;

import br.unicap.ib.exceptions.ClienteExistenteException;
import br.unicap.ib.exceptions.ClienteInexistenteException;
import br.unicap.ib.negocio.Cliente;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author uira
 */
public class RepositorioClienteSet implements IRepositorioClientes {
    
    private Set<Cliente> clientes;

    public RepositorioClienteSet() {
        clientes = new TreeSet<>();
    }

    @Override
    public void inserir(Cliente cliente) throws ClienteExistenteException {
        if(!existe(cliente.getCpf())) {
            clientes.add(cliente);
        } else {
            throw new ClienteExistenteException();
        }
    }

    @Override
    public void atualizar(Cliente cliente) throws ClienteInexistenteException {
        if(clientes.contains(cliente)) {
            clientes.remove(cliente);
            clientes.add(cliente);
        } else {
            throw new ClienteInexistenteException();
        }
    }

    @Override
    public void remover(String cpf) throws ClienteInexistenteException {
        Cliente c = consultar(cpf);
        clientes.remove(c);
    }

    @Override
    public Cliente consultar(String cpfCliente) throws ClienteInexistenteException {
        Iterator<Cliente> it = clientes.iterator();

        Cliente c;
        while(it.hasNext()){
           c = it.next();
           if(c.getCpf().equals(cpfCliente)){
               return c;
           }
        }
        
        throw new ClienteInexistenteException();
    }

    @Override
    public boolean existe(String cpfCliente) {
        Iterator<Cliente> it = clientes.iterator();

        Cliente c;
        while(it.hasNext()){
           c = it.next();
           if(c.getCpf().equals(cpfCliente)){
               return true;
           }
        }
        return false;
    }

    @Override
    public List<Cliente> getAll() {
        List<Cliente> lista = new ArrayList<>();
        Iterator<Cliente> it = clientes.iterator();
        while (it.hasNext()) {
            Cliente next = it.next();
            lista.add(next);
        }
        return lista;
    }
    
}
