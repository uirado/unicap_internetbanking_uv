package br.unicap.ib.dados;

import br.unicap.ib.negocio.Cliente;
import br.unicap.ib.exceptions.ClienteExistenteException;
import br.unicap.ib.exceptions.ClienteInexistenteException;
import java.lang.reflect.Array;
import java.util.List;

/**
 *
 * @author uira
 */
public interface IRepositorioClientes {
 
    public void inserir(Cliente cliente) throws ClienteExistenteException;
    
    public void atualizar(Cliente cliente) throws ClienteInexistenteException;
    
    public void remover(String cpf) throws ClienteInexistenteException;
    
    public Cliente consultar (String cpfCliente) throws ClienteInexistenteException;
    
    public boolean existe(String cpfCliente);

    public List<Cliente> getAll();
}
