package br.unicap.ib.dados;

import br.unicap.ib.exceptions.ContaExistenteException;
import br.unicap.ib.exceptions.ContaInexistenteException;
import br.unicap.ib.gui.JdbcUtil;
import br.unicap.ib.negocio.ContaAbstrata;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author uira
 */
public class RepositorioContasJDBC implements IRepositorioContas {
    
    private Connection con;

    public RepositorioContasJDBC() {
        con = JdbcUtil.getConnection();
    }
    
    @Override
    public void inserir(ContaAbstrata conta) throws ContaExistenteException {
        try {
            String sql = "INSERT INTO tb_conta (tb_cliente_cpf, numero, saldo) values ( ? , ? , ? )";
            
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, conta.getCliente().getCpf());
            stmt.setString(2, conta.getNumero());
            stmt.setDouble(3, conta.getSaldo());
            stmt.executeUpdate();
            stmt.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(RepositorioContasJDBC.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void atualizar(ContaAbstrata conta) throws ContaInexistenteException {
            
        try {
            String sql = "UPDATE tb_conta set (tb_cliente_cpf = ?, numero = ?, saldo = ?) where numero = ?";
            
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, conta.getCliente().getCpf());
            stmt.setString(2, conta.getNumero());
            stmt.setDouble(3, conta.getSaldo());
            stmt.executeUpdate();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(RepositorioContasJDBC.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public void remover(String numeroConta) throws ContaInexistenteException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ContaAbstrata consultar(String numeroConta) throws ContaInexistenteException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean existe(String numeroConta) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
