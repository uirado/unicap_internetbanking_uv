package br.unicap.ib.dados;

import br.unicap.ib.exceptions.ClienteExistenteException;
import br.unicap.ib.exceptions.ClienteInexistenteException;
import br.unicap.ib.gui.JdbcUtil;
import br.unicap.ib.negocio.Cliente;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author uira
 */
public class RepositorioClientesJDBC implements IRepositorioClientes {
    private Connection con;

    public RepositorioClientesJDBC() {
        con = JdbcUtil.getConnection();
    }
    
    
    @Override
    public void inserir(Cliente cliente) throws ClienteExistenteException {
        try {
            String sql = "INSERT INTO tb_cliente (cpf, nome) values ( ? , ? )";
            
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, cliente.getCpf());
            stmt.setString(2, cliente.getNome());
            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(RepositorioContasJDBC.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void atualizar(Cliente cliente) throws ClienteInexistenteException {
        try {
            String sql = "UPDATE tb_cliente SET nome = ? WHERE cpf = ?";
            
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, cliente.getNome());
            stmt.setString(2, cliente.getCpf());
            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(RepositorioContasJDBC.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void remover(String cpf) throws ClienteInexistenteException {
        try {
            String sql = "DELETE FROM tb_cliente WHERE cpf = ?";
            
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, cpf);
            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(RepositorioContasJDBC.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public Cliente consultar(String cpfCliente) throws ClienteInexistenteException {
        Cliente cliente = null;
        try {
            String sql = "SELECT nome, cpf FROM tb_cliente WHERE cpf = ?";
            
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, cpfCliente);
            
            ResultSet result = stmt.executeQuery();
            
            if(result.next()){
                cliente = new Cliente(result.getString("cpf"), result.getString("nome"));
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(RepositorioContasJDBC.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cliente;
    }

    @Override
    public boolean existe(String cpfCliente) {
        Cliente cliente = null;
        try {
            cliente = consultar(cpfCliente);
            if(cliente != null) {
                return true;
            }
        } catch (ClienteInexistenteException ex) {
            Logger.getLogger(RepositorioClientesJDBC.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public List<Cliente> getAll() {
        Cliente cliente = null;
        List<Cliente> lista = new ArrayList<>();
        try {
            String sql = "SELECT nome, cpf FROM tb_cliente";
            
            PreparedStatement stmt = con.prepareStatement(sql);
            ResultSet result = stmt.executeQuery();

            while(result.next()){
                cliente = new Cliente(result.getString("cpf"), result.getString("nome"));
                lista.add(cliente);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(RepositorioContasJDBC.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }
    
}
