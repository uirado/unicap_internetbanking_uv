/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

/**
 *
 * @author uira
 */

public class MeuThread implements Runnable {
    String nome;

    public MeuThread(String nome) {
        this.nome = nome;
    }

    @Override
    public void run() {
        for(int i = 0; i < 3; i++){
            double randomico = Math.random() * 100;
            long gerado = Math.round(randomico);
            System.out.println(this.nome + " gerou valor: " + gerado);
        }
    }
    
//    public static void main(String args[]) {
//        
//        Thread t1 = new Thread(new MeuThread("thread1"));
//        Thread t2 = new Thread(new MeuThread("thread2"));
//        Thread t3 = new Thread(new MeuThread("thread3"));
//        Thread t4 = new Thread(new MeuThread("thread4"));
//        
//        t1.start();
//        t2.start();
//        t3.start();
//        t4.start();
//        
//    }
    
}