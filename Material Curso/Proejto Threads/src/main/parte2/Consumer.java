package main.parte2;


public class Consumer extends Thread {
    private MailBox mailbox;
    private int number;

    public Consumer(MailBox c, int number) {
        mailbox = c;
        this.number = number;
    }

    public void run() {
        for (int i = 0; i < 100; i++) {
            mailbox.get(this.number);
        }
    }
}

