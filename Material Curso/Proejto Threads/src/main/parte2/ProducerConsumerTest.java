package main.parte2;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ProducerConsumerTest {
    public static void main(String[] args) {
        
        ExecutorService pool1 = Executors.newSingleThreadExecutor();
        ExecutorService pool2 = Executors.newSingleThreadExecutor();

    	MailBox c = new MailBox();
        
    	Producer p1 = new Producer(c, 1);
        Producer p2 = new Producer(c, 2);
        Producer p3 = new Producer(c, 3);
        Producer p4 = new Producer(c, 4);
        
        Consumer c1 = new Consumer(c, 1);
        Consumer c2 = new Consumer(c, 2);
        Consumer c3 = new Consumer(c, 3);
        Consumer c4 = new Consumer(c, 4);
        
        pool1.execute(p1);
        pool1.execute(p2);
        pool1.execute(p3);
        pool1.execute(p4);
        
        pool2.execute(c1);
        pool2.execute(c2);
        pool2.execute(c3);
        pool2.execute(c4);
        
        pool1.shutdown();
        pool2.shutdown();
        
//
//        p1.start();
//        c1.start();
//        p2.start();
//        c2.start();

    }
}
