package main.parte2;

public class MailBox {

    private String mensagem;
    
    public MailBox(){
        mensagem = "";
    }

    public synchronized String get(int id) {
        while(mensagem.isEmpty()){
            try {
                wait();
            } catch (InterruptedException ex) {}
        }
        String retorno = "";
        retorno = mensagem;
        mensagem = "";
        System.out.println("Consumer #" + id  + " got: " + retorno);
        notifyAll();
        return retorno;
    }

    public synchronized void put(String value, int id) {
        while(!mensagem.isEmpty()){
            try {
                wait();
            } catch (InterruptedException ex) {}
            
        }
        mensagem = value;
        System.out.println("Producer #" + id + " put: " + mensagem);
        notifyAll();
    }
}
