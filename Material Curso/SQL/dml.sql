insert into tb_cliente values('00000000000', 'Catapóbio da Silva');
insert into tb_cliente values('11111111111', 'Saponácio José');
insert into tb_cliente values('22222222222', 'Zuquim ambim');
insert into tb_cliente values('33333333333', 'Claustrífico tongo');
insert into tb_cliente values('44444444444', 'Astrogilda Tambaemba');
insert into tb_cliente values('55555555555', 'Austin Contrófico');
insert into tb_cliente values('66666666666', 'Oniqua Carbenha');
insert into tb_cliente values('77777777777', 'Pablo Carlo');
insert into tb_cliente values('88888888888', 'Tayrone Bascles');
insert into tb_cliente values('99999999999', 'Coniglio Cenoura');

insert into tb_endereco values('00000000000', '52060000', '0', 'Apto 0');
insert into tb_endereco values('11111111111', '52060111', '1', 'Apto 1');
insert into tb_endereco values('22222222222', '52060222', '2', 'Apto 2');
insert into tb_endereco values('33333333333', '52060333', '3', 'Apto 3');
insert into tb_endereco values('44444444444', '52060444', '4', 'Apto 4');
insert into tb_endereco values('55555555555', '52060555', '5', 'Apto 5');
insert into tb_endereco values('66666666666', '52060666', '6', 'Apto 6');
insert into tb_endereco values('77777777777', '52060777', '7', 'Apto 7');
insert into tb_endereco values('88888888888', '52060888', '8', 'Apto 8');
insert into tb_endereco values('99999999999', '52060999', '9', 'Apto 9');

insert into tb_gerente (nome,fone,celular,email) values('Gerente Caustrofico', '1111-1111','9911-1111','a@qib.com.br');
insert into tb_gerente (nome,fone,celular,email) values('Gerente Blontifico', '2222-2222','9922-2222','b@qib.com.br');
insert into tb_gerente (nome,fone,celular,email) values('Gerente Domplemico', '3333-3333','9933-3333','c@qib.com.br');

insert into tb_gerentes_cliente values(1,'00000000000');
insert into tb_gerentes_cliente values(1,'11111111111');
insert into tb_gerentes_cliente values(1,'22222222222');
insert into tb_gerentes_cliente values(2,'33333333333');
insert into tb_gerentes_cliente values(2,'44444444444');
insert into tb_gerentes_cliente values(2,'55555555555');
insert into tb_gerentes_cliente values(3,'66666666666');
insert into tb_gerentes_cliente values(3,'77777777777');
insert into tb_gerentes_cliente values(3,'88888888888');
insert into tb_gerentes_cliente values(3,'99999999999');

insert into tb_conta (tb_cliente_cpf,numero,saldo) values('00000000000', '0000-0', 2500.00);
insert into tb_conta (tb_cliente_cpf,numero,saldo) values('00000000000', '0000-1', 100.00);
insert into tb_conta (tb_cliente_cpf,numero,saldo) values('00000000000', '0000-2', 100.00);
insert into tb_conta (tb_cliente_cpf,numero,saldo) values('11111111111', '1111-0', 800.00);
insert into tb_conta (tb_cliente_cpf,numero,saldo) values('22222222222', '2222-0', 900.00);
insert into tb_conta (tb_cliente_cpf,numero,saldo) values('33333333333', '3333-0', 70.00);
insert into tb_conta (tb_cliente_cpf,numero,saldo) values('44444444444', '4444-0', 90.00);
insert into tb_conta (tb_cliente_cpf,numero,saldo) values('55555555555', '5555-0', 3800.00);
insert into tb_conta (tb_cliente_cpf,numero,saldo) values('66666666666', '6666-0', 4000.00);
insert into tb_conta (tb_cliente_cpf,numero,saldo) values('77777777777', '7777-0', 50000.00);
insert into tb_conta (tb_cliente_cpf,numero,saldo) values('88888888888', '8888-0', 170.00);
