use ib_db;

DROP TABLE tb_gerentes_cliente;
DROP TABLE tb_endereco;
DROP TABLE tb_conta;
DROP TABLE tb_gerente;
DROP TABLE tb_cliente;

CREATE TABLE tb_cliente  ( 
	cpf CHAR(11) NOT NULL unique,
	nome VARCHAR(100) NOT NULL,
	PRIMARY KEY(cpf)
);
	
CREATE TABLE tb_gerente  ( 
	id INTEGER NOT NULL auto_increment,
	nome VARCHAR(100) NOT NULL,
	fone VARCHAR(20) NOT NULL,
	celular VARCHAR(20) NOT NULL,
	email VARCHAR(50) NOT NULL,
	PRIMARY KEY(id) 
);

CREATE TABLE tb_conta (
	id INTEGER NOT NULL auto_increment,
	tb_cliente_cpf CHAR(11) NOT NULL,
	numero VARCHAR(10) NOT NULL,
	saldo DECIMAL(16,4) NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY(tb_cliente_cpf) REFERENCES tb_cliente(cpf) ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE tb_endereco (
	tb_cliente_cpf CHAR(11) NOT NULL,
	CEP CHAR(8) NULL,
	numero CHAR(5) NULL,
	Complemento CHAR(100) NULL,
	PRIMARY KEY(tb_cliente_cpf),
	FOREIGN KEY(tb_cliente_cpf) REFERENCES tb_cliente(cpf) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE  TABLE tb_gerentes_cliente (
	tb_gerente_id INTEGER NOT NULL,
	tb_cliente_cpf CHAR(11) NOT NULL,
	PRIMARY KEY(tb_gerente_id, tb_cliente_cpf),
	FOREIGN KEY(tb_gerente_id) REFERENCES tb_gerente(id) ON DELETE NO ACTION ON UPDATE NO ACTION,
	FOREIGN KEY(tb_cliente_cpf) REFERENCES tb_cliente(cpf) ON DELETE NO ACTION ON UPDATE NO ACTION
);